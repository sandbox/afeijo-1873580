<?php
class term_list_page_context_condition extends context_condition {

  /**
  * Build a configuration form. Each condition needs this, it's required by Context. You can
  * leave it empty, like this one.
  */
  function condition_form($context) {
    $options = range(1,3);
    return array('term_list' => array(
      '#type' => 'select',
      '#options' => drupal_map_assoc($options),
      '#title' => 'Menu depth',
      '#description' => 'The menu depth where this condition shall run',
    ));
  }

  /**
  * Configuration form submit function, also required by Context.
  */
  function condition_form_submit($values) {
    return array('term_list' => $values['term_list'], );
  }

  /**
  * To check whether a Context condition is active, we need to check for it on every page load.
  * This function isn't called by Context automatically; we call this function later on
  * with a hook_init().
  */
  function execute() {
    $term_list_page = '';

    if ($this->condition_used()) {
      if (arg(0)=='taxonomy') {
        $depth = term_depth(arg(2));
        foreach ($this->get_contexts() as $context) {
          $values = $this->fetch_from_context($context, 'values');
          $term_list_page = $depth == $values['term_list'];
          if ($term_list_page) {
            $this->condition_met($context);
          }
        }
      }
    }
  }
}
